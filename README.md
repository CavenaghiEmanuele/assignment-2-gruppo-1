# UNiMiB Library (Assignment 2 - Gruppo 1) 

La relazione, in formato PDF, è nella cartella principale con il nome:
>  ***Elicitation strategy UniMiB Library.pdf***

NB: La versione pronta per la consegna è quella nel **branch master** e **solo quella**. 
Le versioni presenti sugli altri branch sono da considerarsi come Work In 
Progress.

# Risposte del questionario 

Le risposte del questionario sono in formato .csv nella cartella
principale con il nome:
>  ***Questionario_UniMiB_Library.csv***

## Membri del gruppo
   
| Nome                  | Matricola |
| ------                | ------ |
| Cavenaghi Emanuele    | 816385 |
| Marchetti Davide      | 815990 | 
| Ritucci Simone        | 813473 | 
| Rizza Simone          | 816298 | 
